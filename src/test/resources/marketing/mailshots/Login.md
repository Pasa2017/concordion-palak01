# Verifying correct username on the dashboard page

To verify that after login is successful and user is redirected to the dashboard page, he is able to see his username on the dashboard page




### [Example](- "basic")


When the user [Concordion-testing](- "#username") with password [Concordion](- "#password") [logs](- "#dashboardvalue = loginAndVerifyUsername(#username, #password)") in to reddit.com then the user should see username [Concordion-testing](- "?=#dashboardvalue") on the dashboard page.