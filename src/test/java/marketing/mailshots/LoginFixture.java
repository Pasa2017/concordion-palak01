package marketing.mailshots;

import common.MyDriver;
import pages.LoginPage;
import org.concordion.integration.junit4.ConcordionRunner;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;

@RunWith(ConcordionRunner.class)
public class LoginFixture extends MyDriver {




    public String loginAndVerifyUsername(String username, String password)
    {


        LoginPage loginpage = PageFactory.initElements(driver, LoginPage.class);


        driver.get("https://www.reddit.com");
        loginpage.getUserNameField().sendKeys(username);
        loginpage.getPasswordField().sendKeys(password);
        loginpage.getLoginButton().click();

        String dashboardValue= loginpage.getUserNamePlaceholder().getText();

        return dashboardValue;




    }


}