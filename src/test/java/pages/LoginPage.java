package pages;


import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class LoginPage {





    @FindBy(name="user")
    private WebElement userNameField;

    @FindBy(name="passwd")
    private WebElement passwordField;

    @FindBy(xpath="//div[@class='submit']/descendant::button[@class='btn']")
    private WebElement loginButton;

    @FindBy(xpath="//div[@id='sr-header-area']/descendant::div[@class='dropdown srdrop']/span")
    private WebElement  subredditDrowdown;

    @FindBy(xpath="//div[@class='drop-choices srdrop inuse']")
    private WebElement subredditdropdownlist;

    @FindBy(xpath="//span[@class='hover pagename redditname']/a")
    private WebElement subredditNamePlaceholder;

    @FindBy(xpath="//div[@id='remember-me']/input")
    private WebElement rememberMeCheckbox;


    @FindBy(xpath="//span[@class='user']/a[text()='Concordion-testing']")
    private WebElement userNamePlaceholder;






    public WebElement getUserNameField()
    {
        return userNameField;
    }

    public WebElement getUserNamePlaceholder()
    {
        return userNamePlaceholder;
    }

    public WebElement getPasswordField()
    {
        return passwordField;
    }

    public WebElement getLoginButton()
    {
        return loginButton;
    }

    public WebElement getSubredditDropdown()
    {
        return subredditDrowdown;
    }

    public List<WebElement> getSubredditDropdownList()
    {
        return subredditdropdownlist.findElements(By.xpath(".//a"));
    }

    public WebElement getSubredditNamePlaceholder()
    {
        return subredditNamePlaceholder;
    }

    public WebElement getRememberMeCheckbox()
    {
        return rememberMeCheckbox;
    }


}
