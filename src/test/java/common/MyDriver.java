package common;

import org.concordion.api.AfterExample;
import org.concordion.api.BeforeExample;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;


public class MyDriver {

    protected WebDriver driver;

    @BeforeExample
    public void launchBrowser() {

        System.setProperty("webdriver.chrome.driver", "C:\\chromedriver\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();

    }

    @AfterExample
    public void tearDown(){
        driver.close();
        driver.quit();
    }


}
